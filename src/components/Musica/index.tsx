import React from "react";

import Section from "../Section";

import data from "../../data.json";

function Musica() {
  const { agenda, title, streamLink } = data.rooms.musica;
  return (
    <Section
      className="musica"
      title={title}
      streamLink={streamLink}
      agenda={agenda}
    ></Section>
  );
}

export default Musica;
