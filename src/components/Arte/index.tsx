import React from "react";

import Section from "../Section";

import data from "../../data.json";

function Arte() {
  const { agenda, title, streamLink } = data.rooms.arte;

  return (
    <Section
      className="arte"
      title={title}
      streamLink={streamLink}
      agenda={agenda}
    ></Section>
  );
}

export default Arte;
