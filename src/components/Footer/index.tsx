import React from "react";

import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";

import "./style.scss";

function Footer() {
  return (
    <Container fluid as="footer">
      <Row>
        <Col>
          <Container>
            <Row>
              <Col className="p-3">
                <p className="text-center">
                  InSIDE Festival - Oltre la quarantena. 2020.
                </p>
              </Col>
            </Row>
          </Container>
        </Col>
      </Row>
    </Container>
  );
}

export default Footer;
