import React from "react";

import Section from "../Section";

import data from "../../data.json";

function Riflessioni() {
  const { agenda, title, streamLink } = data.rooms.riflessioni;
  return (
    <Section
      className="riflessioni"
      title={title}
      streamLink={streamLink}
      agenda={agenda}
    ></Section>
  );
}

export default Riflessioni;
