import React from "react";

import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";

import { Link } from "react-router-dom";

import background from "./bg.png";

import "./style.scss";

const Casa: React.FunctionComponent<{}> = (props) => {
  return (
    <div>
      <Container
        key="casa"
        className="casa"
        style={{ backgroundImage: `url(${background})` }}
      >
        <Row className="attico">
          <Col>
            <Link className="musica text-hide" to="/musica" title="Musica">
              Musica
            </Link>
          </Col>
        </Row>
        <Row className="primo-piano">
          <Col xs={6}>
            <Link
              className="riflessioni text-hide"
              to="/riflessioni"
              title="Riflessioni sul futuro"
            >
              Riflessioni
            </Link>
          </Col>
          <Col xs={6}>
            <Link className="cultura text-hide" to="/cultura" title="Cultura">
              Cultura
            </Link>
          </Col>
        </Row>
        <Row className="piano-terra">
          <Col xs={5}>
            <Link className="arte text-hide" to="/arte" title="Fare arte">
              Arte
            </Link>
          </Col>
          <Col xs={{ span: 5, offset: 2 }}>
            <Link className="bimbi text-hide" to="/bimbi" title="Kids act">
              Kids Act
            </Link>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Casa;
