import React from "react";

import Section from "../Section";

import data from "../../data.json";

function Bimbi() {
  const { agenda, title, streamLink } = data.rooms.bimbi;

  return (
    <Section
      className="bimbi"
      title={title}
      streamLink={streamLink}
      agenda={agenda}
    ></Section>
  );
}

export default Bimbi;
