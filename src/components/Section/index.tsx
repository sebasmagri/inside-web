import React from "react";

import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import ResponsiveEmbed from "react-bootstrap/ResponsiveEmbed";
import Row from "react-bootstrap/Row";

import ReactPlayer from "react-player";

import { Link } from "react-router-dom";

import Modal from "react-modal";

import "./style.scss";

Modal.setAppElement("#root");

interface SectionAgendaItem {
  when: string;
  what: string;
  who: string;
}

interface SectionAgenda {
  items: Array<SectionAgendaItem>;
}

interface SectionProps {
  title: string;
  streamLink: string;
  agenda: SectionAgenda;
  className: string;
}

const Section: React.FunctionComponent<SectionProps> = (props) => {
  const { title, streamLink, agenda, className } = props;

  const [agendaIsOpen, setAgendaIsOpen] = React.useState(false);
  function openAgenda() {
    setAgendaIsOpen(true);
  }

  function closeAgenda() {
    setAgendaIsOpen(false);
  }

  return (
    <Container className={`casa ${className} section`}>
      <Row className="title">
        <Col>
          <h2>{title}</h2>
        </Col>
      </Row>
      <Row className="stream">
        <Col>
          <ResponsiveEmbed aspectRatio="16by9">
            <ReactPlayer
              url={streamLink}
              playing
              width="100%"
              height="100%"
              controls={false}
              volume={1}
            />
          </ResponsiveEmbed>
        </Col>
      </Row>
      <Row className="agenda">
        <Col xs={6}>
          <p>
            <a
              className="btn btn-link btn-sm"
              href="javascript:void(0)"
              onClick={openAgenda}
            >
              Agenda
            </a>
            <Modal
              isOpen={agendaIsOpen}
              onRequestClose={closeAgenda}
              contentLabel="Agenda"
              style={{
                content: {
                  top: "1rem",
                  left: "1rem",
                  right: "1rem",
                  bottom: "2rem",
                  border: "3px solid black",
                  background: "#fff",
                  borderRadius: "0",
                  padding: "2rem",
                },
              }}
            >
              <Container fluid>
                <Row>
                  <Col>
                    <header>
                      <h3>Programma della stanza {title}</h3>
                    </header>
                  </Col>
                  <Col xs={2} className="text-right">
                    <Button onClick={closeAgenda} title="Chiudi il programma">
                      x
                    </Button>
                  </Col>
                </Row>
                <Row>
                  <p>
                    <ul className="agenda-list">
                      {agenda.items.map((item) => {
                        return (
                          <li>
                            <span className="when">{item.when}</span> -{" "}
                            <span className="who">
                              <strong>{item.who}</strong>
                            </span>{" "}
                            <span className="what">{item.what}</span>
                          </li>
                        );
                      })}
                    </ul>
                  </p>
                </Row>
              </Container>
            </Modal>
          </p>
        </Col>
        <Col xs={6}>
          <p>
            <Link to="/" className="btn btn-link btn-sm">
              Inizio
            </Link>
          </p>
        </Col>
      </Row>
    </Container>
  );
};

export default Section;
