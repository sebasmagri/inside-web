import React from "react";

import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";

import { Link } from "react-router-dom";

import logo from "./logo.png";

import "./style.scss";

function Header() {
  return (
    <div className="header-wrapper">
      <Container>
        <Row as="header" className="main-header">
          <Col
            xs={4}
            className="logo"
            style={{ backgroundImage: `url(${logo})` }}
          >
            <Link to="/">
              <h1 className="text-hide">InSIDE</h1>
              <p className="text-hide">Oltre la quarantena</p>
            </Link>
          </Col>
          <Col xs={{ span: 5, offset: 3 }} className="text-right date">
            <div
              className="d-flex justify-content-center flex-column"
              style={{ height: "100%" }}
            >
              <p>
                <time dateTime="2020-05-02T15:00:00.000+2">02/05/2020</time>
              </p>
              <p>15h > 24h</p>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default Header;
