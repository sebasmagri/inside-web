import React from "react";

import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";

import { Link } from "react-router-dom";

import data from "../../data.json";

export default function Partecipa() {
  return (
    <Container as="article">
      <Row as="section" className=" mt-4 mb-4">
        <Col>
          <header>
            <h2>Partecipa!</h2>
          </header>
          <p>
            L'evento si svolgerà in modo interattivo sulla piattaforma{" "}
            <a href="https://zoom.us" target="_blank">
              Zoom
            </a>
            .
          </p>
          <p>
            <strong>
              I collegamenti per partecipare saranno disponibili solo atraverso
              la durata del evento.
            </strong>
          </p>
          <p>
            Clicca sul collegamento della stanza che preferisci sotto per unirti
            e partecipare.
          </p>
          <p>
            <ul>
              {Object.entries(data.rooms).map(([roomLabel, room]) => {
                return (
                  <li key={roomLabel}>
                    <strong>{room.title}</strong>:{" "}
                    {room.zoomLink !== null ? (
                      room.zoomLink
                    ) : (
                      <small>
                        (disponibile solo sabato 2 maggio dalle ore 14.45 alle
                        24)
                      </small>
                    )}
                  </li>
                );
              })}
            </ul>
          </p>
          <p>
            <Link className="btn btn-link" to="/">
              Torna al inizio
            </Link>
          </p>
        </Col>
      </Row>
    </Container>
  );
}
