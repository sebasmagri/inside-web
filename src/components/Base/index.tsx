import React from "react";

import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";

import { useLocation } from "react-router-dom";

import backgroundSection from "./bg-section.png";

import "./style.scss";

const Base: React.FunctionComponent<{}> = (props) => {
  const location = useLocation();
  const isRoot = location.pathname === "/";

  return (
    <Container
      as="section"
      className={`base-container ${location.pathname.slice(1)} ${
        isRoot ? "main" : ""
      }`}
    >
      <Row>
        <Col>
          <Container
            className="base-wrapper"
            style={{
              backgroundImage: `url(${backgroundSection})`,
            }}
          >
            <div className="base" id="base">
              {props.children}
            </div>
          </Container>
        </Col>
      </Row>
    </Container>
  );
};

export default Base;
