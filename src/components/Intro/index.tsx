import React from "react";

import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";

import { Link } from "react-router-dom";

export default function Intro() {
  return (
    <Container className="p-3">
      <Row>
        <Col as="section" className="text-center">
          <header>
            <h3>InSIDE, Oltre la quarantena</h3>
            <h5>
              Il più grande evento digitale di arte e cultura di Como e
              dintorni.
            </h5>
          </header>
          <p>Aiutaci a sostenere le realtà culturali presenti al festival</p>
          <p className="text-center">
            <a
              href="https://www.gofundme.com/f/associazioni-e-artisti-comaschi"
              target="_blank"
              rel="noopener noreferrer"
              className="btn btn-link"
            >
              Dona ora!
            </a>
          </p>
        </Col>
      </Row>
      <Row className="mt-4">
        <Col as="section">
          <header className="text-center mb-3">
            <h2>Come posso partecipare?</h2>
          </header>
          <Row>
            <Col xs={6} className="text-center">
              <p>
                <Link to="/partecipa" className="btn btn-link">
                  Entra su Zoom!
                </Link>
              </p>
            </Col>
            <Col xs={6} className="text-center">
              <p>
                <a href="#base" className="btn btn-link">
                  Guarda le live!
                </a>
              </p>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row className="casa-title mt-5">
        <Col className="text-center">
          <h3>Clicca sulla stanza che preferisci!</h3>
        </Col>
      </Row>
    </Container>
  );
}
