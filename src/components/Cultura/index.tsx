import React from "react";

import Section from "../Section";

import data from "../../data.json";

function Cultura() {
  const { agenda, title, streamLink } = data.rooms.cultura;

  return (
    <Section
      className="cultura"
      title={title}
      streamLink={streamLink}
      agenda={agenda}
    ></Section>
  );
}

export default Cultura;
