import React from "react";
import { HashRouter as Router, Switch, Route } from "react-router-dom";

import "./App.scss";

import Arte from "./components/Arte";
import Base from "./components/Base";
import Bimbi from "./components/Bimbi";
import Casa from "./components/Casa";
import Cultura from "./components/Cultura";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Intro from "./components/Intro";
import Musica from "./components/Musica";
import Partecipa from "./components/Partecipa";
import Riflessioni from "./components/Riflessioni";

function App() {
  setTimeout(() => {
    console.warn("refreshing...");
    window.location.reload();
  }, 1000 * 60 * 30);

  return (
    <Router>
      <Header />
      <Switch>
        <Route path="/arte">
          <Base>
            <Arte />
          </Base>
        </Route>
        <Route path="/bimbi">
          <Base>
            <Bimbi />
          </Base>
        </Route>
        <Route path="/cultura">
          <Base>
            <Cultura />
          </Base>
        </Route>
        <Route path="/musica">
          <Base>
            <Musica />
          </Base>
        </Route>
        <Route path="/riflessioni">
          <Base>
            <Riflessioni />
          </Base>
        </Route>
        <Route path="/partecipa">
          <Partecipa />
        </Route>
        <Route path="/">
          <Intro />
          <Base>
            <Casa />
          </Base>
        </Route>
      </Switch>

      <Footer />
    </Router>
  );
}

export default App;
